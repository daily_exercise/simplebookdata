public class Main {
    public static void main(String[] args) {
        Book b1 = new Book();
        Book b2 = new Book();
        Book b3 = new Book();
        Book b4 = new Book();
        Book b5 = new Book();

        b1.setBookId(1);
        b1.setBookName("JAVA");
        b1.setAuthorName("dfg");
        b1.setBookISBN(875453251);
        b1.setPublishDate(1992, 04, 01);

        b2.setBookId(2);
        b2.setBookName("C++");
        b2.setAuthorName("yui");
        b2.setBookISBN(985567256);
        b2.setPublishDate(2000, 01, 31);

        b3.setBookId(3);
        b3.setBookName("Python");
        b3.setAuthorName("erw");
        b3.setBookISBN(567766223);
        b3.setPublishDate(2006, 10, 21);

        b4.setBookId(4);
        b4.setBookName("SQL");
        b4.setAuthorName("xsd");
        b4.setBookISBN(231278755);
        b4.setPublishDate(2003, 06, 12);

        b5.setBookId(5);
        b5.setBookName("C#");
        b5.setAuthorName("mnk");
        b5.setBookISBN(998788883);
        b5.setPublishDate(1999, 10, 10);

        System.out.println("Book ID: " + b1.getBookId() + " | Auther NAme: " + b1.getAuthorName());
        System.out.println("Book ID: " + b2.getBookId() + " | Auther NAme: " + b2.getAuthorName());
        System.out.println("Book ID: " + b3.getBookId() + " | Auther NAme: " + b3.getAuthorName());
        System.out.println("Book ID: " + b4.getBookId() + " | Auther NAme: " + b4.getAuthorName());
        System.out.println("Book ID: " + b5.getBookId() + " | Auther NAme: " + b5.getAuthorName());

    }
}
