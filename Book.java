import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class Book {
    private int bookId;
    private String bookName;
    private String authorName;
    private long bookISBN;
    private String bookPublishedDate;

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBookId() {
        return this.bookId;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookName() {
        return this.bookName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public void setBookISBN(long bookISBN) {
        this.bookISBN = bookISBN;
    }

    public long getBookISBN() {
        return this.bookISBN;
    }

    public void setPublishDate(int year, int motnth, int date) {
        LocalDate localDate = LocalDate.of(year, motnth, date);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formatted = localDate.format(dateTimeFormatter);
        this.bookPublishedDate = formatted;
    }

    public String getPublishDate() {
        return this.bookPublishedDate;
    }

}